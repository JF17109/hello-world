﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hello_world
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }
        //Configuración radioButtons
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
           
            label1.Text = "HOLA";
            Image imagenHola = Image.FromFile("HOLA.PNG");
            pictureBox1.Image = imagenHola;

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
     
            label1.Text = "ADIÓS";
            Image imagenAdios = Image.FromFile("ADIOS.PNG");
            pictureBox1.Image = imagenAdios;
        }
    }
}
